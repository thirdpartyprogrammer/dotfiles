Dotfiles
========

Linux dotfiles for various programs.


What's changed?
---------------

Previously, this worked as a git bare repository.

I find it cleaner, however, to work by symlinking files from the dotfiles 
directory, for multiple reasons.

For one, I've had to use a git alias, which works fine when I set it in my 
.zshrc file, but didn't work well at all with zsh autocompletion or tpope's 
Fugitive plugin.

For second, using 'git checkout', adding dotfiles to my system was all or nothing.
It would fail if I was already using a different zshrc file, for example.


What it allows
--------------

By using an included command-line utility, `dotlink`, picking and choosing what 
works for each system is an extremely easy task. 

If I need a dotfile that's specific for my Raspberry Pi, but not for a standard
linux distribution, including that file in the repository is no problem.


How to use it
-------------

### Using `dotlink`

Clone this as a standard git repository wherever you like (I put mine in `~/dotfiles`).
Put this in your bashrc or zshrc file, providing a full path to the directory.
Also, make sure `~/.local/bin` is in your path if you plan to install `dotlink`.

```sh
# in `~/.zshrc` or `~/.bashrc`

export DOTFILES_DIR="$HOME/dotfiles"
export PATH="$PATH:/$HOME/.local/bin"
```

Next, go to the directory and install `dotlink` to your system.
Dotlink will be installed to `~/.local/bin`, creating it if it doesn't exist.

```sh
cd $HOME/dotfiles
./dotlink install
```

### Using symlinks

Dotlink uses symlinks! You're perfectly fine using just symlinks too.

```sh
# To use vim dotfiles, for example...

ln -s ~/dotfiles/.vim/ ~/.vim
```


Minecraft Mods
--------------

I use Forge to load my mods, so all mods are Forge-compatible. I use the MultiMC
launcher, but they should work well with most or all minecraft launchers, including
the official one. You should be able to create a profile in the launcher and 
copy these mods accordingly to your .minecraft/mods/ folders.


Contributing
------------

Do you have any ideas on what may better organize the project?
Any interesting contributions or new dotfiles for your favorite programs?

Feel free to submit a pull request or open an issue!


License
-------
This project is MIT licensed, see the LICENSE file for more information.
