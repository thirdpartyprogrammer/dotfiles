" Syntax highlighting
syntax on
filetype plugin indent on
set termguicolors

colorscheme badwolf

set background=dark

" highlight Normal guibg=NONE

set hlsearch

set hidden

" Show a verticle line to constrain code
set colorcolumn=80

" More room for status:
" Less 'ENTER to continue' prompts
set cmdheight=3

" Autoload changes in files
" Used so gofmt changes automatically show
set autoread

" Indentation (4 spaces, smart)
set noexpandtab
set tabstop=4
set smartindent
set shiftwidth=4

" Number line
set number
set relativenumber
set numberwidth=10

" No text wrapping
set nowrap

" No bells
set belloff=all

" Preserve undo history
set undofile
set undodir=~/.vim/undodir

set shortmess+=c

" Set Mapleader
let mapleader = ','

" Set Emmet's leader key
let g:user_emmet_leader_key = '<C-Z>'

" Set up ALE to autoformat source code
let g:ale_fixers = { 'elixir': ['mix_format'] }

" Set up YouCompleteMe
let g:ycm_global_ycm_extra_conf = $HOME . '/.vim/plugged/YouCompleteMe/third_party/ycmd/.ycm_extra_conf.py'

" For rust.vim plugin
let g:rustfmt_autosave = 1

let g:slime_target = 'vimterminal'

let g:rainbow_active = 1
