" Shaders
autocmd! BufNewFile,BufRead *.shader set filetype=glsl

" EJS Syntax
autocmd! BufNewFile,BufRead *.ejs set filetype=html

" Javascript Programming
augroup lang_js
	autocmd!
	augroup FileType javascript set filetype=typescript " For flow-js syntax highlighting
augroup END

" C Programming
augroup lang_c
    autocmd!
    autocmd FileType c nnoremap <buffer> <Leader>cc :C2H<CR>
    autocmd FileType c nnoremap <buffer> <Leader>ed :Termdebug<CR>
    autocmd FileType c setlocal cindent
    autocmd FileType c setlocal foldmethod=syntax
	autocmd FileType c FocusDispatch './build.sh'
    " autocmd BufNewFile,BufRead *.h setlocal filetype=c
	autocmd BufWritePost *.c execute '!clang-format -i -style=Microsoft ' . shellescape(expand('%'))
augroup END

" C++ Programming
augroup lang_cpp
    autocmd!
    autocmd FileType cpp nnoremap <buffer> <Leader>cc :C2H<CR>
    autocmd FileType cpp nnoremap <buffer> <Leader>ed :Termdebug<CR>
    autocmd FileType cpp setlocal cindent
    autocmd FileType cpp setlocal foldmethod=syntax
	autocmd FileType cpp FocusDispatch './build.sh'
    autocmd BufNewFile,BufRead *.h setlocal filetype=cpp
	" autocmd BufWritePost *.cpp execute '!clang-format -i -style=Microsoft ' . shellescape(expand('%'))
	" autocmd BufWritePost *.h execute '!clang-format -i -style=Microsoft ' . shellescape(expand('%'))
augroup END

" Rust Programming
augroup lang_rust
    autocmd!
    autocmd FileType rust let b:dispatch = 'cargo run'
	autocmd BufNewFile,BufRead *.wgsl setlocal filetype=rust 
augroup END

augroup lang_vim
    autocmd!
    autocmd FileType vim nnoremap <buffer> <Leader>, :PlugUpdate<CR>
    autocmd FileType vim nnoremap <buffer> <Leader>. :PlugClean<CR>
augroup END

" Go Programming
augroup lang_go
    autocmd!
    autocmd FileType go let b:start = 'go run ' .. shellescape(expand('%'))
augroup END
