" Automatically install plugin manager
if empty(glob('~/.vim/autoload/plug.vim'))
  silent execute '!curl -fLo ~/.vim/autoload/plug.vim --create-dirs  https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin()

Plug 'kien/ctrlp.vim' " Fuzzy finder
Plug 'tpope/vim-vinegar' " Filesystem
Plug 'tpope/vim-dispatch' " Debugging
Plug 'tpope/vim-fugitive' " Git wrapper
Plug 'tpope/vim-rhubarb' " Github plugin for Fugitive
Plug 'ap/vim-css-color' " Highlight colors
Plug 'szw/vim-maximizer' " Windows
Plug 'tpope/vim-surround' " Manipulate quotes, parens, etc.
Plug 'tpope/vim-commentary' " Comment/uncomment lines
Plug 'mattn/emmet-vim' " Snippets for web development
Plug 'jiangmiao/auto-pairs' " Auto pairs
Plug 'linluk/vim-c2h' " Switch between source and header files
Plug 'sheerun/vim-polyglot'
Plug 'morhetz/gruvbox'
Plug 'rose-pine/vim', { 'as': 'rose-pine' }
Plug 'neoclide/coc.nvim', {'do': { -> coc#util#install()}}
Plug 'neoclide/coc-snippets', {'do': 'yarn install --frozen-lockfile'}
Plug 'neoclide/coc-highlight', {'do': 'yarn install --frozen-lockfile'} " color highlighting
Plug 'rust-lang/rust.vim'
Plug 'ziglang/zig.vim'
Plug 'rainglow/vim', {'as':'rainglow'}
Plug 'drewtempelmeyer/palenight.vim'
Plug 'sjl/badwolf/'
" Plug 'yuezk/vim-js'
Plug 'leafgarland/typescript-vim'
Plug 'rescript-lang/vim-rescript'
Plug 'neoclide/jsonc.vim'
Plug 'jpalardy/vim-slime'
" Plug 'kovisoft/slimv'
Plug 'luochen1990/rainbow'
call plug#end()
