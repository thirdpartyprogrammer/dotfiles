vim.opt.hidden = true -- Allow for hidden buffers
vim.opt.autoread = true -- Autoload changes in files

vim.opt.termguicolors = true

vim.opt.colorcolumn = "80"
vim.opt.hlsearch = true -- Highlight search options

vim.opt.cmdheight = 3 -- More room for messages


-- Smart indentation, 4 tabs
vim.opt.expandtab = false
vim.opt.smartindent = true
vim.opt.tabstop = 4
vim.opt.shiftwidth = 0 -- Inherit the tabstop value

vim.opt.number = true
vim.opt.relativenumber = true
vim.opt.numberwidth = 10

vim.opt.wrap = false

vim.opt.belloff = "all"

vim.opt.undofile = true
vim.opt.undodir= os.getenv("HOME") .. "/.config/nvim/undodir"

vim.g.sonokai_style = "shusia"
vim.cmd [[ colorscheme sonokai ]]

vim.g.slime_target = "tmux"

