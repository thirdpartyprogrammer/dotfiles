return {
	-- Aesthetics
	"sainnhe/sonokai",
	"luochen1990/rainbow",
	"ap/vim-css-color",
	"nvim-treesitter/nvim-treesitter",

	-- LSP
	"williamboman/mason.nvim",
	"williamboman/mason-lspconfig.nvim",
	"neovim/nvim-lspconfig",
	"hrsh7th/cmp-nvim-lsp",
	"hrsh7th/cmp-buffer",
	"hrsh7th/cmp-path",
	"hrsh7th/cmp-cmdline",
	"hrsh7th/nvim-cmp",
	"hrsh7th/cmp-vsnip",
	"hrsh7th/vim-vsnip",

	-- File management
	"tpope/vim-vinegar",
	"kien/ctrlp.vim",

	-- Buffers
	"szw/vim-maximizer",

	-- Editing
	"tpope/vim-commentary",
	"tpope/vim-surround",
	"windwp/nvim-autopairs",

	-- C/C++
	"linluk/vim-c2h",
	"bfrg/vim-cpp-modern",

	-- Haxe
	"jdonaldson/vaxe",

	-- Nim
	"zah/nim.vim",

	-- Web Development
	"mattn/emmet-vim",

	-- REPL
	"jpalardy/vim-slime",

	-- Debugging
	"tpope/vim-dispatch",

	-- Git
	"tpope/vim-fugitive",
}
