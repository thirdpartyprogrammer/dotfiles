-- LSP-config mappings (global and via on_attach)
--
local nmap = require("thirdpartyprogrammer.maps").nmap
function nmapbuf(key, cmd, bufnr)
	local opts = { noremap = true, silent = true, buffer = bufnr }
	vim.keymap.set("n", key, cmd, opts)
end

nmap("<space>e", vim.diagnostic.open_float)
nmap("[d", vim.diagnostic.goto_prev)
nmap("]d", vim.diagnostic.goto_next)
nmap("<space>q", vim.diagnostic.setloclist)

local on_attach = function(client, bufnr)
  vim.api.nvim_buf_set_option(bufnr, "omnifunc", "v:lua.vim.lsp.omnifunc")

  nmapbuf("gD", vim.lsp.buf.declaration, bufnr)
  nmapbuf("gd", vim.lsp.buf.definition, bufnr)
  nmapbuf("K", vim.lsp.buf.hover, bufnr)
  nmapbuf("gi", vim.lsp.buf.implementation, bufnr)
  -- nmapbuf("<C-k>", vim.lsp.buf.signature_help, bufnr) -- Using C-k in maps neovim config already
  nmapbuf("<space>wa", vim.lsp.buf.add_workspace_folder, bufnr)
  nmapbuf("<space>wr", vim.lsp.buf.remove_workspace_folder, bufnr)
  nmapbuf("<space>wl", function()
    print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
  end, bufnr)
  nmapbuf("<space>D", vim.lsp.buf.type_definition, bufnr)
  nmapbuf("<space>rn", vim.lsp.buf.rename, bufnr)
  nmapbuf("<space>ca", vim.lsp.buf.code_action, bufnr)
  nmapbuf("gr", vim.lsp.buf.references, bufnr)
  nmapbuf("<space>f", function() vim.lsp.buf.format { async = true } end, bufnr)
end

-- Nvim-cmp (completion engine)
--
local cmp = require"cmp"

cmp.setup({
	snippet = {
		expand = function(args)
			vim.fn["vsnip#anonymous"](args.body)
		end,
	},
	mapping = cmp.mapping.preset.insert({
		["<C-b>"] = cmp.mapping.scroll_docs(-4),
		["<C-f>"] = cmp.mapping.scroll_docs(4),
		["<C-Space>"] = cmp.mapping.complete(),
		["<C-e>"] = cmp.mapping.abort(),
		["<CR>"] = cmp.mapping.confirm({ select = true }),
	}),
	sources = cmp.config.sources({
		{ name = "nvim_lsp" },
		{ name = "vsnip" },
	}, {
		{ name = "buffer" },
	})
})

cmp.setup.filetype("gitcommit", {
	sources = cmp.config.sources({
		{ name = "cmp_git" },
	}, {
		{ name = "buffer" },
	})
})

cmp.setup.cmdline({ "/", "?" }, {
	mapping = cmp.mapping.preset.cmdline(),
	sources = {
		{ name = "buffer" }
	}
})

cmp.setup.cmdline(":", {
	mapping = cmp.mapping.preset.cmdline(),
	sources = cmp.config.sources({
		{ name = "path" }
	}, {
		{ name = "cmdline" }
	})
})

-- Mason (loader for LSP, DAP, etc.)
--
require("mason").setup()
require("mason-lspconfig").setup()

require("mason-lspconfig").setup_handlers({
	function (server_name)
		local capabilities = require("cmp_nvim_lsp").default_capabilities()
		require("lspconfig")[server_name].setup({
			capabilities = capabilities,
			on_attach = on_attach,
		})
	end,
})

-- Autopairs
--
require("nvim-autopairs").setup()
