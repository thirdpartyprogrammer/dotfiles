-- Set global variables
--
vim.g.mapleader = " "

require("thirdpartyprogrammer.plugins")
require("thirdpartyprogrammer.pluginsetup")
require("thirdpartyprogrammer.maps")
require("thirdpartyprogrammer.settings")
