-- Map functions --
--
local mapopts = { 
	noremap = true,
	silent = true,
}

function map(mode, key, cmd) vim.keymap.set(mode, key,cmd, mapopts) end
function nmap(key, cmd) map("n", key, cmd) end
function vmap(key, cmd) map("v", key, cmd) end
function tmap(key, cmd) map("t", key, cmd) end

-- Movement/window mappings --
--
nmap("<C-h>", "<C-w><C-h>")                                           -- Move to a left pane
nmap("<C-j>", "<C-w><C-j>")                                           -- Move to a bottom pane
nmap("<C-k>", "<C-w><C-k>")                                           -- Move to a top pane
nmap("<C-l>", "<C-w><C-l>")                                           -- Move to a right pane
nmap("<Leader>r", ":execute 'e ' .. fnameescape(expand(\"%'\"))<CR>") -- Reload current buffer
nmap("mm", ":MaximizerToggle<CR>")                                    -- Maximize current window (vim-maximizer)
nmap("mM", "<C-w>H<C-w><C-w>mm<C-w><C-w><C-w><C-w>")                  -- Swap right window with left and maximize (vim-maximizer)
nmap("<C-n>", ":nohlsearch<CR>")                                      -- Clear highlighting temporarily

-- Config file mappings --
--
local lua_config_dir = os.getenv("HOME") .. "/.config/nvim/lua/thirdpartyprogrammer"

nmap("<Leader>sv", ":source $MYVIMRC<CR>")                           -- Edit vim config in new tab
nmap("<Leader>ev", ":tabedit " .. lua_config_dir .. "<CR>")          -- Source vim config

-- Plugin mappings --
--
nmap("mm", ":MaximizerToggle<CR>")                                    -- Maximize current window (vim-maximizer)
nmap("mM", "<C-w>H<C-w><C-w>mm<C-w><C-w><C-w><C-w>")                  -- Swap right window with left and maximize (vim-maximizer)

-- Tab mappings --
--
nmap("<Leader>t", ":tabnew<CR>")                                      -- Open a new tab
nmap("<S-h>", ":tabprev<CR>")                                         -- Switch to left tab
nmap("<S-l>", ":tabnext<CR>")                                         -- Switch to right tab
nmap("<Leader>ff", ":Lexplore 20<CR>")                                -- Open file explorer

-- Debug mappings
--
nmap("<Leader>,", ":Start<CR>")                                       -- Run in background (vim-dispatch)
nmap("<Leader>.", ":Dispatch!<CR>")                                   -- Run in quickfix window (vim-dispatch)

-- Switch between source/header files in C
--
nmap("<Leader>cc", ":C2H<CR>")

-- Terminal mappings
--
nmap("tt", ":terminal<CR>")                                           -- Open a terminal
tmap("<C-o>", "<C-\\><C-n>")                                          -- Go to normal mode from terminal mode

return { 
	map = map,
	nmap = nmap,
	vmap = vmap,
	tmap = tmap,
}
