# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
setopt autocd
unsetopt beep
bindkey -v
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/jarville/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall

# Custom configuration
export PATH=$PATH:$HOME/.local/bin/

alias s="source $HOME/.zshrc"
alias ls="ls --color"

alias ddg="w3m https://duckduckgo.com"
alias mftut="w3m https://makefiletutorial.com"
PS1="%U%B%F{blue}%n%u%f@%m(%F{cyan}%1~%f) %# %b"

export LD_LIBRARY_PATH=/usr/lib 
export EDITOR="vim"

# alias tmux="TERM=screen-256color-bce tmux"

export DOTFILES_DIR="$HOME/dotfiles"
alias startvnc="vncserver :1 -geometry 1600x720"

# For Andronix, which starts VNC :1 with an undefined DISPLAY
if [ -z $DISPLAY ]; then
	export DISPLAY=:1
fi

# pnpm
export PNPM_HOME="$HOME/.local/share/pnpm"
export PATH="$PNPM_HOME:$PATH"
# pnpm end

if [ -z "$HOME/.cargo/env" ]; then
	source "$HOME/.cargo/env"
fi

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

export TERM="xterm-256color"

# Golang
export PATH=$PATH:/usr/local/go/bin

# Zig
export PATH=$PATH:$HOME/.zig
export PATH=$PATH:$HOME/.zls/bin

export MC="$HOME/.local/share/multimc/instances/1.19.3/.minecraft"

# asdf
. "$HOME/.asdf/asdf.sh"
fpath=(${ASDF_DIR}/completions $fpath)

export PATH=$PATH:$HOME/.nimble/bin
export PATH=$HOME/.asdf/shims:$PATH

alias sbcl="rlwrap sbcl"

export PATH=$PATH:$HOME/.local/share/lua-language-server/bin/

alias ldtk="$HOME/ldtk/ldtk.AppImage"

# bun completions
[ -s "/home/chris/.bun/_bun" ] && source "/home/chris/.bun/_bun"

# bun
export BUN_INSTALL="$HOME/.bun"
export PATH="$BUN_INSTALL/bin:$PATH"
